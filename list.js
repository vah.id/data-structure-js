//Link list 
function node(val){
    this.data = val;
    this.link = null;
}
class LinkedList{
    constructor(){
        this.first = null;
    }
    insFirst(val){
        let p = new node(val);
        p.data = val;
        p.link = this.first;
        this.first = p;
        return this;
    }
    insMiddle(n,val){
        let p = new node(val);
        p.link = n.link;
        n.link = p;
        return this;
    }
    insEnd(val){
        let p = new node(val);
        let q = this.first;
        while(q.link != null){
            q = q.link;
        }
        p.link = q.link;
        q.link = p;
        return this;
    }
    
    showList(){
        let q = this.first;
        let out = '';
        while(q != null){
            out = out.concat(q.data);
            out = out.concat(" > ");
            q = q.link;
        }
        out = out.concat(" null");
        return out;
    }
    removeFromList(val){
        let p = this.first;
        if(p === null)
            return "List is empty!";
        if(p.data === val){
            this.first = this.first.link;
            return this;
        }
        let prevNode = this.first;
        let thisNode = prevNode.link;
        
        while(thisNode){
            if(thisNode.data === val)
                break;
            prevNode = thisNode;
            thisNode = thisNode.link;
        }
        if(thisNode === null){
            return "Node not found!";
        }
        prevNode.link = thisNode.link;
        return this;
    }
}

