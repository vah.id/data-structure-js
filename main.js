//Functions Main file

//Infix to Postfix
function inToPost(inputText){
    inputText = document.getElementById("infix").value;
    var s1 = new stack();
    var out = '';
    
        for(let i = 0;i <= inputText.length; i++){
            let ch = inputText.substr(i,1);
            
            if(ch == '('){
                s1.push(ch);
            }
            else if(ch == ')')
            {
                while (s1.elements[s1.top] != '('){
                    out = out.concat(s1.pop());
                }
                s1.pop();
            }
            else if(isOperator(ch))
            {
                if( pri(ch) <= pri(s1.elements[s1.top]) ){
                    out = out.concat(s1.pop());
                    s1.push(ch);
                }else{
                    s1.push(ch);
                }
            }
            else
            {
                out = out.concat(ch);
            }
        }
    while(s1.top > -1 ){
        out = out.concat(s1.pop());
    }
    document.getElementById("postfix").innerHTML = out;
    return out;
    }

//Check Symmetry
function Symmetry(value){
    value = document.getElementById("sym").value;
    let q = new queue();
    let s = new stack();
    let check = true;
    
    for(let i=0;i<=value.length-1;i++){
        let ch = value.substr(i,1);
        q.insQ(ch);
        s.push(ch);
    }
    for(let i=0;i<(value.length/2);i++){
        let c1 = q.delQ();
        let c2 = s.pop();
        
        if(c1 != c2){
            check = false;
        }
    }
    if(check)
        document.getElementById("symcheck").innerHTML = "This text is Symmetry!!";
    else
        document.getElementById("symcheck").innerHTML = "This text is NOT Symmetry!!";
    return check;
}
//Add to a link list
var linklist = new LinkedList();

function addToList(ls,val){
    ls = linklist;
    val = document.getElementById("nval").value;
    if(ls.first === null)
        ls.insFirst(val);
    else
        ls.insEnd(val);
    document.getElementById("nodeoflist").innerHTML = ls.showList();
}
//Add a 12 after all 4 in link list
function addAfter4(list){
    list = linklist;
    let q = list.first;
    while(q){
        if(q.data == 4){
            list.insMiddle(q,12);
        }
        q = q.link;
    }
    document.getElementById("nodeoflist2").innerHTML = list.showList();
}
var btree = new BSTtree();
function addToTree(tree){
    tree = btree;
    var value = document.getElementById("addbst").value;
    value = parseInt(value);
    tree.addNode(value);
}
function showInorder(tree){
    tree = btree;
    document.getElementById("inorder").innerHTML = tree.inOrder();
}