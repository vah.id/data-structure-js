//Stack class structure
class stack {
    constructor() {
        this.top = -1;
        this.elements = [];
    }

    push(value) {
        this.elements.push(value);
        this.top++;
        return "Pushed";
    }
    pop() {
        this.top--;
        return this.elements.pop();
    }
    show() {
        for (let i = this.top; i > -1; i--) {
            return this.elements[i];
        }
    }
    isEmpty() {
        if (this.top == -1) {
            return true;
        } else {
            return false;
        }
    }
}

function isOperator(value) {
    if (value == '*' || value == '/' || value == '+' || value == '-') {
        return true
    }
    return false;
}

function pri(value) {
    if (value == '*' || value == '/')
        return 2;
    if (value == '+' || value == '-')
        return 1;
    return 0;
}