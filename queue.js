//Queue class structure

class queue{
    constructor(){
        this.front = -1;
        this.rear = -1;
        this.items = [];
    }
    insQ(value){
        this.items[++this.rear] = value;
    }
    delQ(){
        if(!this.isEmpty()){
            return this.items[++this.front];
        }
        return "Queue is empty!!";
    }
    showQ(){
        if(!this.isEmpty()){
            for(let i=this.front+1;i<=this.rear;i++){
                console.log(this.items[i]);
            }
            return ;
        }
        return "Queue is empty!!";
    }
    isEmpty(){
        if(this.rear == this.front){
            return true;}
        return false;
    }
    
}