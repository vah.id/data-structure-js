//Tree class structure
function tNode(val){
    this.lChild = null;
    this.data = val;
    this.rChild= null;
}
class BSTtree{
    constructor(root){
        this.root = null;
    }
    
    addNode(val){
        var p = this.root;
        while(true){
            if(p == null){
                this.root = new tNode(val);
                break;
            }else if(val < p.data){
                if(p.lChild == null){
                    p.lChild = new tNode(val);
                    p = p.lChild;
                    p.lChild = null;
                    p.rChild = null;
                    break;
                }else if(p.lChild != null ){
                    p = p.lChild;
                }
            }else if(val > p.data){
                if(p.rChild == null){
                    p.rChild = new tNode(val);
                    p = p.rChild;
                    p.lChild = null;
                    p.rChild = null;
                    break;
                }else if(p.rChild != null ){
                    p = p.rChild;
                }
            }
            else if(val == p.data){
                console.log("This value is exists!!");
                break;
            }
        }
        return this;
    }
    isEmpty(){
        if (!this.root){
            return true;
        }
        return false;
    }
    inOrder(){
        let tmpNode = this.root;
        var outStack = new stack();
        let out = '';
        if(this.root){
            while(1){
                while(tmpNode){
                    outStack.push(tmpNode);
                    tmpNode = tmpNode.lChild;
                }
                if(! outStack.isEmpty()){
                    tmpNode = outStack.pop();
                    out = out.concat(tmpNode.data);
                    out = out.concat(" - ");
                    tmpNode = tmpNode.rChild;
                }else
                    break;
            }
        }
        return out;
    }
}
